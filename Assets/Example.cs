﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;
using System.Text;
using System.Net;
using System.IO;
using BackEnd;

public class Example : MonoBehaviour
{
    public Text text;
    public static Example instance;

    string client_id = "2950306028338377";
    string secret_id = "0d33d07d536b2751eee766f59347849c";
    string org_scoped_id = "";
    string code = "";
    string oauth_token = "";

    const string postValue = "code={0}&access_token=OC|{1}|{2}&org_scoped_id={3}";
    public string url;
    // Start is called before the first frame update
    void Awake()
    {
        if (instance == null)
            instance = this;
    }
    void Start()
    {
        Backend.Initialize(() =>
        {
            if (Backend.IsInitialized)
                Debug.Log("이니셜 성공");
        });

        Debug.Log(Backend.Utils.GetGoogleHash());

    }

    // Update is called once per frame
    void Update()
    {
        text.text = url;
        //Debug.Log(Backend.OculusAuth.GetURLtoOculusToken());
    }
    public void OnCheckSSOPath(string url)
    {

        //string oc = Backend.OculusAuth.Get(url);

        // if (url.Contains("redirect_uri")) // 혹시 모르니 
        //     return;

        // if (!url.Contains("#")) // #이 없으면 base64가 나온게 아니니 리턴
        //     return;

        // Debug.Log("들어왔음");
        // string[] str = url.Split(new char[] { '#' }); // #을 기준으로 sso_uri와 base64를 분리
        // string base64key = str[1]; // base64 지정

        // GetCodeAndScopedId(base64key);
        // PostOuathToken();
        // Debug.Log("토큰값:" + oauth_token);
        string oc = Backend.OculusAuth.GetOculusTokenbyURL(url);
        Debug.Log(Backend.BMember.AuthorizeFederation(oc, FederationType.Oculus, ""));
        Debug.Log(Backend.BMember.GetUserInfo());

    }

    void GetCodeAndScopedId(string base64key)
    {
        byte[] byte64 = Convert.FromBase64String(base64key); // base64를 byte로 변경
        string result = Encoding.UTF8.GetString(byte64); // byte들을 string으로 변경

        result = result.Substring(1, result.Length - 2); // 괄호 제거
        result = result.Replace("\"", ""); // 따옴표 제거
        result = result.Replace(",org_scoped_id", ""); // 기호로 안지워지는 거 제거

        string[] info = result.Split(new char[] { ':' }); // : 를 통해 code(영어),code(숫자),org_scoped_id로 나눔

        //info[0]에는 string code가 들어감
        code = info[1]; // code(숫자)
        org_scoped_id = info[2]; // org_scoped_id
        Debug.Log("code:" + code);
        Debug.Log("org_scoped_id" + org_scoped_id);
    }

    string PostOuathToken()
    {
        string value = String.Format(postValue, code, client_id, secret_id, org_scoped_id);
        Debug.Log("1차 출력 : " + value);
        string url = "https://graph.oculus.com/sso_authorize_code?";

        HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
        request.ContentType = "application/x-www-form-urlencoded; charset=UTF-8";
        request.Method = "POST";

        byte[] bytearray = Encoding.UTF8.GetBytes(value);
        request.ContentLength = bytearray.Length;

        Stream stream = request.GetRequestStream();
        stream.Write(bytearray, 0, bytearray.Length);
        stream.Close();

        WebResponse webResponse;

        try
        {
            webResponse = request.GetResponse();
            stream = webResponse.GetResponseStream();
        }
        catch (WebException exception)
        {
            string responseText;

            using (var reader = new StreamReader(exception.Response.GetResponseStream()))
            {
                responseText = reader.ReadToEnd();
                Debug.Log("에러 : " + responseText);
            }
        }
        //using (WebResponse webResponse = request.GetResponse())
        //{

        StreamReader streamReader = new StreamReader(stream);
        string sPreturn = streamReader.ReadToEnd();

        streamReader.Close();
        stream.Close();

        string result = sPreturn.Substring(1, sPreturn.Length - 2); // 대괄호 제거
        result = result.Replace("\"", "");
        result = result.Replace("oauth_token:", "");

        string[] info = result.Split(new char[] { ',' });

        oauth_token = info[0];

        Debug.Log("성공:" + oauth_token);

        return oauth_token;
    }
}
